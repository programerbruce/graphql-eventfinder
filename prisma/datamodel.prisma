type File {
  id: ID! @unique
  createdAt: DateTime!
  updatedAt: DateTime!
  filename: String!
  mimetype: String!
  encoding: String!
  url: String! @unique
  type: String!,
  referenceId: String!
}

type User {
  id: ID! @id
  name: String!
  email: String! @unique
  password: String!
  age: Int
  img: String
  location: Location
  hostedEvents: [Event!]! @relation(name: "HostedEvents", onDelete: CASCADE)
  joinedEvents: [Event!]! @relation(name: "EventMembers", onDelete: CASCADE)
  submittedPendingRequest: [PendingRequest!] @relation(onDelete: CASCADE)
  pushNotificationTokens: [PushNotificationTokens!]!
  createdAt: DateTime! @createdAt
  updatedAt: DateTime! @updatedAt
}

type Location {
  id: ID! @id
  address: String!
  lat: Float!
  lng: Float!
  short_des: String!
  suburb: Suburb
  createdAt: DateTime! @createdAt
  updatedAt: DateTime! @updatedAt
}

type Suburb {
  id: ID! @id
  postcode: Int!
  name: String!
  state: String!
  lat: Float!
  lng: Float!
  dc: String!
  createdAt: DateTime! @createdAt
  updatedAt: DateTime! @updatedAt
}

type Category {
  id: ID! @id
  name: String!
  img: String!
  createdAt: DateTime! @createdAt
  updatedAt: DateTime! @updatedAt
}

type PersonalChatRoom {
  id: ID! @id
  members: [User]!
  msgs: [Message]!
  lstLeavingTime: [LastLeavingTime]!
  createdAt: DateTime! @createdAt
  updatedAt: DateTime! @updatedAt
}

type Message {
  id: ID! @id
  content: String!
  createdAt: DateTime! @createdAt
  updatedAt: DateTime! @updatedAt
}

type LastLeavingTime {
  id: ID! @id
  member: User!
  time: DateTime!
  createdAt: DateTime! @createdAt
  updatedAt: DateTime! @updatedAt
}

type Event {
  id: ID! @id
  owner: User! @relation(name: "HostedEvents")
  name: String!
  imgs: [String] @scalarList(strategy: RELATION)
  description: String
  start: DateTime!
  end: DateTime!
  categories: [Category]
  members: [User] @relation(name: "EventMembers" onDelete: CASCADE)
  chatRoom: GroupChatRoom @relation(name: "EventChatRoom", onDelete: CASCADE)
  pendingRequests: [PendingRequest] @relation(onDelete: CASCADE)
  locations: [Location] @relation(name: "EventLocations", onDelete: CASCADE)
  comments: [Comment]
  createdAt: DateTime! @createdAt
  updatedAt: DateTime! @updatedAt
}

type NotificationCategory {
  id: ID! @id
  type: String!
  notifications: [Notification]!
  createdAt: DateTime! @createdAt
  updatedAt: DateTime! @updatedAt
}

type Notification {
  id: ID! @id
  type: String!
  owners: [User]!
  msg: String!
  createdAt: DateTime! @createdAt
  updatedAt: DateTime! @updatedAt
}

type PushNotificationTokens {
  id: ID! @id
  token: String!
  createdAt: DateTime! @createdAt
  updatedAt: DateTime! @updatedAt
}

type GroupChatRoom {
  id: ID! @id
  msgs: [Message]!
  lstLeavingTime: [LastLeavingTime]!
  createdAt: DateTime! @createdAt
  updatedAt: DateTime! @updatedAt
}

type PendingRequest {
  id: ID! @id
  content: String!
  createdAt: DateTime! @createdAt
  updatedAt: DateTime! @updatedAt
}

type Comment {
  id: ID! @id
  content: String!
  approved: Boolean! @default(value: false)
  thumUps: [ThumUp]!
  reports: [Report]!
  createdAt: DateTime! @createdAt
  updatedAt: DateTime! @updatedAt
}

type ThumUp {
  id: ID! @id
  owner: User!
  createdAt: DateTime! @createdAt
  updatedAt: DateTime! @updatedAt
}

type Report {
  id: ID! @id
  owner: User!
  createdAt: DateTime! @createdAt
  updatedAt: DateTime! @updatedAt
}
