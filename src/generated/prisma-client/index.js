"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var prisma_lib_1 = require("prisma-client-lib");
var typeDefs = require("./prisma-schema").typeDefs;

var models = [
  {
    name: "User",
    embedded: false
  },
  {
    name: "Location",
    embedded: false
  },
  {
    name: "Suburb",
    embedded: false
  },
  {
    name: "Category",
    embedded: false
  },
  {
    name: "PersonalChatRoom",
    embedded: false
  },
  {
    name: "Message",
    embedded: false
  },
  {
    name: "LastLeavingTime",
    embedded: false
  },
  {
    name: "Event",
    embedded: false
  },
  {
    name: "NotificationCategory",
    embedded: false
  },
  {
    name: "Notification",
    embedded: false
  },
  {
    name: "PushNotificationTokens",
    embedded: false
  },
  {
    name: "GroupChatRoom",
    embedded: false
  },
  {
    name: "PendingRequest",
    embedded: false
  },
  {
    name: "Comment",
    embedded: false
  },
  {
    name: "ThumUp",
    embedded: false
  },
  {
    name: "Report",
    embedded: false
  }
];
exports.Prisma = prisma_lib_1.makePrismaClientClass({
  typeDefs,
  models,
  endpoint: `http://localhost:4466`
});
exports.prisma = new exports.Prisma();
