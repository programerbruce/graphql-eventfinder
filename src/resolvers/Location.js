function suburb (parent, args, context) {
  return context.prisma.location({ id: parent.id }).suburb()
}

module.exports = {
  suburb
}
