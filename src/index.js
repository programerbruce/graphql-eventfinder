const { GraphQLServer } = require('graphql-yoga')
const { Prisma } = require('prisma-binding')

const cors = require('cors')

const Query = require('./resolvers/Query')
const Mutation = require('./resolvers/Mutation')
const Event = require('./resolvers/Event')
const Location = require('./resolvers/Location')

const resolvers = {
  Query,
  Mutation,
  Event,
  Location
}


const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: request => ({
    ...request,
    db: new Prisma({
      typeDefs: 'src/generated/prisma.graphql',
      endpoint: 'http://localhost:4466/prisma-s3/dev',
      secret: 'mysecret123',
      debug: true,
    }),
  }),
})

server.start(() => console.log(`Server is running on http://localhost:4000`))
