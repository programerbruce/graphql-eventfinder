const uuid = require('uuid/v1')
const aws = require('aws-sdk')

const s3 = new aws.S3({
  accessKeyId: 'AKIAIB2BWFEBMX2ASIPQ',
  secretAccessKey: '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',
  params: {
    Bucket: 'react-native-juyou'
  },
  endpoint: new aws.Endpoint('https://s3-ap-southeast-2.amazonaws.com/react-native-juyou') // fake s3 endpoint for local dev
})

exports.processUpload = async ( upload, ctx ) => {
  if (!upload) {
    return console.log('ERROR: No file received.')
  }

  const { stream, filename, mimetype, encoding, type, referenceId } = await upload
  const key = uuid() + '-' + filename

  // Upload to S3
  const response = await s3
    .upload({
      Key: key,
      ACL: 'public-read',
      Body: stream
    }).promise()

  const url = response.Location

  // Sync with Prisma
  const data = {
    filename,
    mimetype,
    encoding,
    url,
    type,
    referenceId
  }

  const { id } = await ctx.db.mutation.createFile({ data }, ` { id } `)

  const file = {
    id,
    filename,
    mimetype,
    encoding,
    url,
    type,
    referenceId
  }

  console.log('saved prisma file:')
  console.log(file)

  return file
}
