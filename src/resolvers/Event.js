function owner(parent, args, context) {
  return context.prisma.event({ id: parent.id }).owner()
}

function locations(parent, args, context) {
  return context.prisma.event({ id: parent.id }).locations()
}

function chatRoom(parent, args, context) {
  return context.prisma.event({ id: parent.id }).chatRoom()
}

function members(parent, args, context) {
  return context.prisma.event({ id: parent.id }).members()
}

function categories(parent, args, context) {
  return context.prisma.event({ id: parent.id }).categories()
}

function pendingRequests(parent, args, context) {
  return context.prisma.event({ id: parent.id }).pendingRequests()
}

module.exports = {
  owner,
  locations,
  chatRoom,
  members,
  categories,
  pendingRequests
}
