const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { APP_SECRET, getUserId } = require('../utils')
const { processUpload } = require('../modules/fileApi')

async function signup(parent, args, context, info) {
  // 1
  const password = await bcrypt.hash(args.password, 10)
  // 2
  const user = await context.prisma.createUser({ ...args, password })

  // 3
  const token = jwt.sign({ userId: user.id }, APP_SECRET)

  // 4
  return {
    token,
    user,
  }
}

async function login(parent, args, context, info) {
  // 1
  const user = await context.prisma.user({ email: args.email })
  if (!user) {
    throw new Error('No such user found')
  }

  // 2
  const valid = await bcrypt.compare(args.password, user.password)
  if (!valid) {
    throw new Error('Invalid password')
  }

  const token = jwt.sign({ userId: user.id }, APP_SECRET)

  // 3
  return {
    token,
    user,
  }
}

async function createEvent(parent, args, context, info) {

  const userId = getUserId(context)

  if (!userId) {
    throw new Error('No such user found')
  }

  const insertedEvent = await context.prisma.createEvent({
    owner: { connect: { id: userId } },
    name: args.name,
    start: args.start,
    end: args.end,
    chatRoom: {
      create: {

      }
    },
    locations: {
      create: args.locations.map(location => {
        return {
          address: location.address,
          lat: location.lat,
          lng: location.lng,
          short_des: location.short_des,
          suburb: { connect: { id: location.suburb } }
        }
      })
    },
    categories: {
      connect: args.categories.map(category => {
        return{
          id: category
        }
      })
    }
  })

  Promise.all(args.files.map(file => {
    file.type = "event"
    file.referenceId = insertedEvent.id
    processUpload(file, ctx)
  }))

  return insertedEvent

}

module.exports = {
  signup,
  login,
  createEvent
}
